﻿namespace PivotalData_RecordingExporter_Tester
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBox_PCServ2 = new System.Windows.Forms.TextBox();
            this.textBox_PCServ1 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox_PCPass = new System.Windows.Forms.TextBox();
            this.textBox_PCUser = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label_ConnectionState = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.button_S3 = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button_UploadRec = new System.Windows.Forms.Button();
            this.button_FetchRec = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.textBox_ServiceEvents = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.button_StartRun = new System.Windows.Forms.Button();
            this.button_EWSTest = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textBox_PCServ2);
            this.groupBox1.Controls.Add(this.textBox_PCServ1);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.textBox_PCPass);
            this.groupBox1.Controls.Add(this.textBox_PCUser);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(13, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(344, 137);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "IC Credentials";
            // 
            // textBox_PCServ2
            // 
            this.textBox_PCServ2.Location = new System.Drawing.Point(238, 98);
            this.textBox_PCServ2.Name = "textBox_PCServ2";
            this.textBox_PCServ2.Size = new System.Drawing.Size(100, 20);
            this.textBox_PCServ2.TabIndex = 7;
            this.textBox_PCServ2.Text = "10.241.31.206";
            // 
            // textBox_PCServ1
            // 
            this.textBox_PCServ1.Location = new System.Drawing.Point(7, 98);
            this.textBox_PCServ1.Name = "textBox_PCServ1";
            this.textBox_PCServ1.Size = new System.Drawing.Size(100, 20);
            this.textBox_PCServ1.TabIndex = 6;
            this.textBox_PCServ1.Text = "10.241.31.206";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(235, 82);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Server 2";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 82);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Server 1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(235, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Password";
            // 
            // textBox_PCPass
            // 
            this.textBox_PCPass.Location = new System.Drawing.Point(238, 37);
            this.textBox_PCPass.Name = "textBox_PCPass";
            this.textBox_PCPass.PasswordChar = '*';
            this.textBox_PCPass.Size = new System.Drawing.Size(100, 20);
            this.textBox_PCPass.TabIndex = 2;
            this.textBox_PCPass.Text = "1234";
            // 
            // textBox_PCUser
            // 
            this.textBox_PCUser.Location = new System.Drawing.Point(7, 37);
            this.textBox_PCUser.Name = "textBox_PCUser";
            this.textBox_PCUser.Size = new System.Drawing.Size(100, 20);
            this.textBox_PCUser.TabIndex = 1;
            this.textBox_PCUser.Text = "pivotal.action";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "User";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label_ConnectionState);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.button1);
            this.groupBox2.Location = new System.Drawing.Point(364, 13);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(200, 137);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "PureConnect Connection";
            // 
            // label_ConnectionState
            // 
            this.label_ConnectionState.AutoSize = true;
            this.label_ConnectionState.Location = new System.Drawing.Point(7, 104);
            this.label_ConnectionState.Name = "label_ConnectionState";
            this.label_ConnectionState.Size = new System.Drawing.Size(35, 13);
            this.label_ConnectionState.TabIndex = 2;
            this.label_ConnectionState.Text = "status";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 82);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(97, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "Connection Status:";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(7, 20);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 40);
            this.button1.TabIndex = 0;
            this.button1.Text = "Connect";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.groupBox5);
            this.groupBox3.Controls.Add(this.groupBox4);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.textBox1);
            this.groupBox3.Location = new System.Drawing.Point(13, 157);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(551, 305);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Recording Processing";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.button_EWSTest);
            this.groupBox5.Controls.Add(this.button_S3);
            this.groupBox5.Location = new System.Drawing.Point(295, 20);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(250, 100);
            this.groupBox5.TabIndex = 4;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "S3 Bucket Isolated Test";
            // 
            // button_S3
            // 
            this.button_S3.Location = new System.Drawing.Point(7, 33);
            this.button_S3.Name = "button_S3";
            this.button_S3.Size = new System.Drawing.Size(75, 36);
            this.button_S3.TabIndex = 0;
            this.button_S3.Text = "Check S3";
            this.button_S3.UseVisualStyleBackColor = true;
            this.button_S3.Click += new System.EventHandler(this.button_S3_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.button3);
            this.groupBox4.Controls.Add(this.button2);
            this.groupBox4.Controls.Add(this.button_UploadRec);
            this.groupBox4.Controls.Add(this.button_FetchRec);
            this.groupBox4.Location = new System.Drawing.Point(10, 20);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(278, 100);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Actions";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(131, 56);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(141, 31);
            this.button3.TabIndex = 3;
            this.button3.Text = "Upload Recordings File";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(6, 56);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(119, 31);
            this.button2.TabIndex = 2;
            this.button2.Text = "Fetch Rec List File";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button_UploadRec
            // 
            this.button_UploadRec.Location = new System.Drawing.Point(131, 19);
            this.button_UploadRec.Name = "button_UploadRec";
            this.button_UploadRec.Size = new System.Drawing.Size(141, 31);
            this.button_UploadRec.TabIndex = 1;
            this.button_UploadRec.Text = "Upload Recordings Byte";
            this.button_UploadRec.UseVisualStyleBackColor = true;
            this.button_UploadRec.Click += new System.EventHandler(this.button_UploadRec_Click);
            // 
            // button_FetchRec
            // 
            this.button_FetchRec.Location = new System.Drawing.Point(6, 19);
            this.button_FetchRec.Name = "button_FetchRec";
            this.button_FetchRec.Size = new System.Drawing.Size(119, 31);
            this.button_FetchRec.TabIndex = 0;
            this.button_FetchRec.Text = "Fetch Rec List Bytes";
            this.button_FetchRec.UseVisualStyleBackColor = true;
            this.button_FetchRec.Click += new System.EventHandler(this.button_FetchRec_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 123);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(71, 13);
            this.label6.TabIndex = 2;
            this.label6.Text = "Event Stream";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(2, 142);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(539, 149);
            this.textBox1.TabIndex = 1;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.textBox_ServiceEvents);
            this.groupBox6.Controls.Add(this.label15);
            this.groupBox6.Controls.Add(this.label14);
            this.groupBox6.Controls.Add(this.label13);
            this.groupBox6.Controls.Add(this.label12);
            this.groupBox6.Controls.Add(this.label11);
            this.groupBox6.Controls.Add(this.label10);
            this.groupBox6.Controls.Add(this.label9);
            this.groupBox6.Controls.Add(this.label8);
            this.groupBox6.Controls.Add(this.label7);
            this.groupBox6.Controls.Add(this.button_StartRun);
            this.groupBox6.Location = new System.Drawing.Point(571, 13);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(369, 449);
            this.groupBox6.TabIndex = 3;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Service Emulation";
            // 
            // textBox_ServiceEvents
            // 
            this.textBox_ServiceEvents.Location = new System.Drawing.Point(6, 236);
            this.textBox_ServiceEvents.Multiline = true;
            this.textBox_ServiceEvents.Name = "textBox_ServiceEvents";
            this.textBox_ServiceEvents.Size = new System.Drawing.Size(357, 199);
            this.textBox_ServiceEvents.TabIndex = 5;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(13, 220);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(71, 13);
            this.label15.TabIndex = 9;
            this.label15.Text = "Event Stream";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(10, 122);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(329, 13);
            this.label14.TabIndex = 8;
            this.label14.Text = "* Retry row failures are stored in retry folder and marked Failed status";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(10, 109);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(291, 13);
            this.label13.TabIndex = 7;
            this.label13.Text = "* Retry rows are decrypted, converted to MP3 and uploaded";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(10, 96);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(340, 13);
            this.label12.TabIndex = 6;
            this.label12.Text = "* Pending row failures are stored in retry folder and marked Retry status";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(10, 82);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(305, 13);
            this.label11.TabIndex = 5;
            this.label11.Text = "* Pending rows are decrypted, converted to MP3 and uploaded";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(10, 69);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(194, 13);
            this.label10.TabIndex = 4;
            this.label10.Text = "* Code fetch of Pending and Retry rows";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(10, 56);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(130, 13);
            this.label9.TabIndex = 3;
            this.label9.Text = "* DB Audit tabel new rows";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(10, 39);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(142, 13);
            this.label8.TabIndex = 2;
            this.label8.Text = "instead of pieces in isolation:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(7, 20);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(309, 13);
            this.label7.TabIndex = 1;
            this.label7.Text = "This starts an attempt in almost the same way the service would ";
            // 
            // button_StartRun
            // 
            this.button_StartRun.Location = new System.Drawing.Point(13, 164);
            this.button_StartRun.Name = "button_StartRun";
            this.button_StartRun.Size = new System.Drawing.Size(75, 37);
            this.button_StartRun.TabIndex = 0;
            this.button_StartRun.Text = "Start";
            this.button_StartRun.UseVisualStyleBackColor = true;
            this.button_StartRun.Click += new System.EventHandler(this.button_StartRun_Click);
            // 
            // button_EWSTest
            // 
            this.button_EWSTest.Location = new System.Drawing.Point(169, 33);
            this.button_EWSTest.Name = "button_EWSTest";
            this.button_EWSTest.Size = new System.Drawing.Size(75, 36);
            this.button_EWSTest.TabIndex = 1;
            this.button_EWSTest.Text = "Test EWS";
            this.button_EWSTest.UseVisualStyleBackColor = true;
            this.button_EWSTest.Click += new System.EventHandler(this.button_EWSTest_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(952, 479);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox textBox_PCServ2;
        private System.Windows.Forms.TextBox textBox_PCServ1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox_PCPass;
        private System.Windows.Forms.TextBox textBox_PCUser;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label_ConnectionState;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button button_UploadRec;
        private System.Windows.Forms.Button button_FetchRec;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button button_S3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TextBox textBox_ServiceEvents;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button button_StartRun;
        private System.Windows.Forms.Button button_EWSTest;
    }
}

