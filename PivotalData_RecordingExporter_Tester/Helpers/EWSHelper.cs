﻿using Microsoft.Exchange.WebServices.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.IO;
using System.Runtime.CompilerServices;

namespace PivotalData_RecordingExporter_Tester.Helpers
{
    public class EWSHelper : IDisposable
    {
        //Creds
        string credEmail = "";
        string credPassword = "";
        string autoDiscoURL = "";
        string exchangeVersion = "";
        
        //Runtime vars
        string _currentTo { get; set; }
        string _currentCC { get; set; }

        ExchangeService _service = new ExchangeService();

        public EWSHelper()
        {
            //Refresh
            System.Configuration.ConfigurationManager.RefreshSection("appSettings");

            //Defaults
            _currentTo = System.Configuration.ConfigurationManager.AppSettings["DefaultToEmail"];
            _currentCC = System.Configuration.ConfigurationManager.AppSettings["DefaultCCEmail"];

            //Creds
            credEmail = System.Configuration.ConfigurationManager.AppSettings["CredEmail"];
            credPassword = System.Configuration.ConfigurationManager.AppSettings["CredPassword"];
            autoDiscoURL = System.Configuration.ConfigurationManager.AppSettings["AutodiscoverURL"];
            exchangeVersion = System.Configuration.ConfigurationManager.AppSettings["ExchangeVersion"];

            if (credPassword.Contains("amp"))
            {
                credPassword = credPassword.Replace("amp", "&");
            }

            //connect();
        }

        public void connect()
        {
            try
            {
                switch (exchangeVersion)
                {
                    case "Exchange2013SP1":
                        _service = new ExchangeService(ExchangeVersion.Exchange2013_SP1);
                        break;
                    case "Exchange2007SP1":
                        _service = new ExchangeService(ExchangeVersion.Exchange2007_SP1);
                        break;
                    case "Exchange2010":
                        _service = new ExchangeService(ExchangeVersion.Exchange2010);
                        break;
                    case "Exchange2010SP1":
                        _service = new ExchangeService(ExchangeVersion.Exchange2010_SP1);
                        break;
                    case "Exchange2010SP2":
                        _service = new ExchangeService(ExchangeVersion.Exchange2010_SP2);
                        break;
                    case "Exchange2013":
                        _service = new ExchangeService(ExchangeVersion.Exchange2013);
                        break;
                    default:
                        _service = new ExchangeService(ExchangeVersion.Exchange2013_SP1);
                        break;
                }

                _service.Credentials = new WebCredentials(credEmail, credPassword);

                //_service.AutodiscoverUrl(autoDiscoURL, RedirectionUrlValidationCallback);
                //Redirected URL = https://vi1p194mb0301.eurp194.prod.outlook.com:444/EWS/Services.wsdl
                //Mark URL = https://outlook.office365.com/EWS/Exchange.asmx
                _service.Url = new Uri("https://outlook.office365.com/EWS/Exchange.asmx");

                StaticFunctions.WriteEventLog("Exchange connection via EWS successfully established, any failure emails should be able to send for this batch of recordings.", System.Diagnostics.EventLogEntryType.Information);

            }
            catch (Exception ce)
            {
                StaticFunctions.WriteEventLog("Failure while trying to connect to Exchange via EWS. All subsequent emails will likely fail however the process will not stop running. Please check event logs for emails that would have failed." + Environment.NewLine +
                                                "Exception caught while trying to connect was: " + Environment.NewLine +
                                                ce.Message, System.Diagnostics.EventLogEntryType.Error);
            }
        }

        public bool SendEmail(string ToEmail, string CCEmail, string Subject, string Message)
        {
            string ToField = "";
            string CCField = "";

            //If To and CC empty, use defaults configured in App.Config
            if (string.IsNullOrEmpty(ToEmail))
            {
                ToField = _currentTo;
            }
            else
            {
                ToField = ToEmail;
            }

            if (string.IsNullOrEmpty(CCEmail))
            {
                CCField = _currentCC;
            }
            else
            {
                CCField = CCEmail;
            }

            try
            {
                EmailMessage email = new EmailMessage(_service);
                email.ToRecipients.Add(ToField);
                email.CcRecipients.Add(CCField);
                email.Subject = Subject;
                email.Body = new MessageBody(Message);
                email.Send();

                return true;
            }
            catch (Exception er)
            {
                StaticFunctions.WriteEventLog("Failure while trying to send an failure email. Please manually intervenet! The following email was not sent: " + Environment.NewLine + 
                                                "To: " + ToField + Environment.NewLine + 
                                                "CC: " + CCField + Environment.NewLine + 
                                                "Subject: " + Subject + Environment.NewLine + 
                                                "Message: " + Message + Environment.NewLine + 
                                                "Exception caught while trying to send was: " + Environment.NewLine + 
                                                er.Message, System.Diagnostics.EventLogEntryType.Error);
                return false;
            }

        }

        private static bool RedirectionUrlValidationCallback(string redirectionUrl)
        {

            bool result = false;
            Uri redirectionUri = new Uri(redirectionUrl);
            if (redirectionUri.Scheme == "https")
            {
                result = true;
            }
            return result;
        }

        public void Dispose()
        {
            _service = null;
        }
    }
}
