﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Amazon;
using Amazon.Runtime;
using Amazon.Runtime.CredentialManagement;
using Amazon.S3;
using Amazon.S3.IO;
using Amazon.S3.Model;
using Amazon.S3.Transfer;

namespace PivotalData_RecordingExporter_Tester.Helpers
{
    public class AmazonS3Functions : IDisposable
    {
        AmazonS3Client s3Client;
        string _bucketName, _client, _secret;
        NetSDKCredentialsFile netSDKFile = new NetSDKCredentialsFile();
        public AmazonS3Functions()
        { 
            _client = System.Configuration.ConfigurationManager.AppSettings["S3AccessKeyId"];
            _secret = System.Configuration.ConfigurationManager.AppSettings["S3SecretAccessKey"];
            _bucketName = System.Configuration.ConfigurationManager.AppSettings["S3Object"];

            AWSCredentials credentials;
            credentials = new BasicAWSCredentials(_client, _secret);

            s3Client = new AmazonS3Client(credentials, Amazon.RegionEndpoint.EUWest1);
        }

        public bool CheckBucketAccess(string folderName)
        {
            try
            {
                S3DirectoryInfo s3Root = new S3DirectoryInfo(s3Client, _bucketName);
                if (s3Root.Exists)
                {
                    var subDir = s3Root.GetDirectory(folderName);
                    if (subDir.Exists)
                    {
                        //yay
                        return true;
                    }
                    else
                    {
                        s3Root.CreateSubdirectory(folderName);
                        return true;
                    }
                    
                }
                else
                {
                    return false;
                }
            }
            catch (Exception be)
            {

                //Bucket failed
                return false;
            }
        }

        public void GetBucketList()
        {
            //Do not have the permissions to do this
            try
            {
                var buckets = s3Client.ListBuckets();
            }
            catch (Exception bfe)
            {

                //Bucket Fetch failed
            }
            
        }

        public string UploadFileBytes(byte[] file, string filename, string folderName)
        {
            try
            {
                TransferUtility utility = new TransferUtility(s3Client);
                TransferUtilityUploadRequest request = new TransferUtilityUploadRequest();

                if (string.IsNullOrEmpty(folderName))
                {
                    request.BucketName = _bucketName; 
                }
                else
                {   
                    request.BucketName = _bucketName + @"/" + folderName;
                }
                request.Key = filename;
                Stream stream = new MemoryStream(file);
                request.InputStream = stream;
                utility.Upload(request);

                return "Success|File Uploaded successfully";
            }
            catch (Exception terr)
            {
                StaticFunctions.WriteEventLog("Failure while trying upload to S3 bucket. This will count as a failure. Filename: " + filename + ", Bucket: " + _bucketName + ". Exception caught was: " + terr.Message, System.Diagnostics.EventLogEntryType.Error);
                return "Failure|Exception caught while trying to upload file " + filename + ", error was: " + terr.Message;
            }
        }

        public void DownloadFile(string path, string key)
        {
            TransferUtility utility = new TransferUtility(s3Client);
            utility.Download(path, _bucketName, "testfile1.wav");
        }

        public void UploadFileUNC(string file)
        { 
        
        }

        private static async Task<ListBucketsResponse> MyListBucketsAsync(IAmazonS3 s3Client)
        {
            return await s3Client.ListBucketsAsync();
        }

        public void Dispose()
        {
            try
            {
                s3Client.Dispose();
                s3Client = null;
            }
            catch (Exception)
            {
            }
        }
    }
}
