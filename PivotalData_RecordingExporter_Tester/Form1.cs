﻿using PivotalData_RecordingExporter_Tester.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PivotalData_RecordingExporter_Tester
{
    public partial class Form1 : Form
    {
        PivotalData_RecordingExporter_Tester.Helpers.IceLibFunctions func1;
        byte[] tempMedia;
        string tempPath;
        public Form1()
        {
            InitializeComponent();
            label_ConnectionState.Text = "";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            label_ConnectionState.Text = "";
            try
            {
                
                func1 = new Helpers.IceLibFunctions(textBox_PCServ1.Text, textBox_PCServ2.Text, textBox_PCUser.Text, textBox_PCPass.Text);

                label_ConnectionState.Text = "Connected!";

                
            }
            catch (Exception err)
            {
                label_ConnectionState.Text = "Failed to connect";
                MessageBox.Show("Exception while trying to connect to PureConnect: " + err);
            }
            
        }

        private void button_FetchRec_Click(object sender, EventArgs e)
        {
            string tempRecId = "E2A05E16-BD69-D010-882B-0901C7400001";

            func1.connectRecorder();

            //func1.UploadRecording(tempRecId);
            var fileBytes = func1.GetMediaBytesFromRecId(tempRecId);

            tempMedia = fileBytes;
        }

        private void button_S3_Click(object sender, EventArgs e)
        {
            try
            {
                Helpers.AmazonS3Functions s3Functions = new Helpers.AmazonS3Functions();
                string folderName = DateTime.Now.ToString("yyyy") + @"/" + DateTime.Now.ToString("MM") + @"/" + DateTime.Now.ToString("dd");
                s3Functions.CheckBucketAccess(folderName);
                //s3Functions.GetBucketList();
                s3Functions.UploadFileBytes(tempMedia, "test.mp3", folderName);
            }
            catch (Exception s3e)
            {

                MessageBox.Show("Failure while interfacing with S3: " + s3e.Message);
            }
        }

        private void button_UploadRec_Click(object sender, EventArgs e)
        {
            if (tempMedia.Length == 0)
            {
                MessageBox.Show("No recording has been fetched to upload");
                return;
            }

            //MP3



            //S3
            try
            {
                Helpers.AmazonS3Functions s3Functions = new Helpers.AmazonS3Functions();

                s3Functions.CheckBucketAccess(DateTime.Now.ToString("yyyy-MM-dd"));
                //s3Functions.GetBucketList();
                s3Functions.UploadFileBytes(tempMedia, "test.mp3", DateTime.Now.ToString("yyyy-MM-dd"));
            }
            catch (Exception s3e)
            {

                MessageBox.Show("Failure while interfacing with S3: " + s3e.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string tempRecId = "E2A05E16-BD69-D010-882B-0901C7400001";

            func1.connectRecorder();

            //func1.UploadRecording(tempRecId);
            Guid tempFilename = Guid.NewGuid();
            var filePath = func1.StoreFileFromRecId(tempRecId, tempFilename.ToString() + ".wav");

            tempPath = filePath;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(tempPath))
            {
                MessageBox.Show("No recording has been fetched to upload");
                return;
            }

            //MP3
            var outcome = Helpers.MP3Functions.WaveToMP3_FileToFile(tempPath, "EndtoEndTest.mp3", 64);


            //S3
            try
            {
                Helpers.AmazonS3Functions s3Functions = new Helpers.AmazonS3Functions();

                s3Functions.CheckBucketAccess(DateTime.Now.ToString("yyyy-MM-dd"));
                //s3Functions.GetBucketList();
                var uploadOutcome = s3Functions.UploadFileBytes(File.ReadAllBytes(outcome), "test.mp3", DateTime.Now.ToString("yyyy-MM-dd"));

                if (uploadOutcome.Contains("Success"))
                {
                    File.Delete(outcome);
                }
                else
                {
                    string failurePath = System.Configuration.ConfigurationManager.AppSettings["S3FailureRetryUNC"];
                    File.Move(outcome, failurePath + Path.GetFileName(outcome));
                }

                

               //Testing
               // s3Functions.DownloadFile(@"C:\Temp\s3work\Download\download1.mp3", "testfile1.mp3");
            }
            catch (Exception s3e)
            {

                MessageBox.Show("Failure while interfacing with S3: " + s3e.Message);
            }
        }

        private void button_StartRun_Click(object sender, EventArgs e)
        {
            bool processRows = false;
            textBox_ServiceEvents.Text = "Starting simulation run" + Environment.NewLine + Environment.NewLine;

            Helpers.DBFunctions dbFunc1 = new Helpers.DBFunctions();
            var connected = dbFunc1.Connect();

            if (connected)
            {
                textBox_ServiceEvents.Text = "Successfully connected to database" + Environment.NewLine + Environment.NewLine;
                StaticFunctions.WriteEventLog("Service successfully connected to database.", System.Diagnostics.EventLogEntryType.Information);
            }
            else
            {
                textBox_ServiceEvents.Text = "Could not connect to database, check credentials and network connectivity" + Environment.NewLine + Environment.NewLine;
                StaticFunctions.WriteEventLog("Service could not connect to database, check credentials and network connectivity. There would have been messages before this indicating why the connection failed. Service will wait for next attempt.", System.Diagnostics.EventLogEntryType.Error);
            }

            if (connected)
            {
                //Proceed
                StaticFunctions.WriteEventLog("Service about to check whether there are any new rows to process.", System.Diagnostics.EventLogEntryType.Information);
                var dataStatus = dbFunc1.PrepareLatestRecordings();

                if (!string.IsNullOrEmpty(dataStatus))
                {
                    if (dataStatus == "Rows Added")
                    {
                        //We have something new to process
                        processRows = true;
                        StaticFunctions.WriteEventLog("Rows found and added to audit table for processing. There will be a run attempt", System.Diagnostics.EventLogEntryType.Information);
                    }
                    else
                    {
                        //No New Rows
                        //Nothing new to process, but there could still be unprocessed rows
                        processRows = true;
                        StaticFunctions.WriteEventLog("No new rows were found, no run attempt will be executed. Service will wait for next attempt.", System.Diagnostics.EventLogEntryType.Information);
                    }
                }

                if (processRows)
                {
                    //There is data to process, fetch it!

                    try
                    {
                        StaticFunctions.WriteEventLog("Service about to attempt fetching of DB rows to process...", System.Diagnostics.EventLogEntryType.Information);
                        var rows = dbFunc1.FetchRecordsToProcess();
                        if (rows != null)
                        {
                            if (rows.Count > 0)
                            {
                                //Sort pending from retry
                                var pendingList = rows.FindAll(x => x.Status == "Pending");
                                var retryList = rows.FindAll(x => x.Status == "Retry");

                                if (pendingList.Count > 0)
                                {
                                    StaticFunctions.WriteEventLog(pendingList.Count.ToString() +  " pending rows about to be processed...", System.Diagnostics.EventLogEntryType.Information);
                                    Helpers.StaticFunctions.ProcessList(pendingList, dbFunc1);
                                    StaticFunctions.WriteEventLog("Pending row processing complete.", System.Diagnostics.EventLogEntryType.Information);
                                }

                                if (retryList.Count > 0)
                                {
                                    StaticFunctions.WriteEventLog(retryList.Count.ToString() + " retry rows about to be processed...", System.Diagnostics.EventLogEntryType.Information);
                                    Helpers.StaticFunctions.ProcessList(retryList, dbFunc1);
                                    StaticFunctions.WriteEventLog("Retry row processing complete.", System.Diagnostics.EventLogEntryType.Information);
                                }
                            }
                            else
                            {
                                //Something went wrong as we got confirmation earlier that there was data to process
                                //Log something and continue
                                StaticFunctions.WriteEventLog("Potential issue in logic, Db functions indicated valid rows to process, however when fetching rows, none were availabe for processing. Not error or exception caught, investigation needed.", System.Diagnostics.EventLogEntryType.Warning);
                            }
                        }

                    }
                    catch (Exception rfe)
                    {
                        //Exception fetching rows
                        StaticFunctions.WriteEventLog("Error while trying to fetch DB rows for processing. Service reaches this point after identifying there are new rows to process but was unable to fetch them from DB. Exception that was caught: " + rfe.Message, System.Diagnostics.EventLogEntryType.Error);
                    }
                }

                //Cleanup
                try
                {
                    //Cleanup, drop DB and nullify class
                    StaticFunctions.WriteEventLog("Run attempt has completed, DB connection and other variables will now be disposed until the next run.", System.Diagnostics.EventLogEntryType.Information);
                    dbFunc1.Disconnect();
                    dbFunc1 = null;
                }
                catch (Exception ce)
                {
                    //
                }
            }
            else
            {
                //Cleanup, drop DB and nullify class
                StaticFunctions.WriteEventLog("Run attempt has unsuccessfull, DB connection never established. Variables will now be disposed until the next run.", System.Diagnostics.EventLogEntryType.Error);
                dbFunc1.Disconnect();
                dbFunc1 = null;
            }

        }

        private void button_EWSTest_Click(object sender, EventArgs e)
        {
            EWSHelper mailClient = new EWSHelper();
            DateTime startTime = DateTime.Now;
            mailClient.connect();
            DateTime endTime = DateTime.Now;
            mailClient.SendEmail("jannie.pretorius@pivotalgroup.co.za", "stephan.heine@pivotalgroup.co.za", "EWS Speed test - Autodiscover", "Test email sent after connect event for Exchange. Start time was: " + startTime.ToString("yyyy-MM-dd HH:mm:ss:fff") + " until end time: " + endTime.ToString("yyyy-MM-dd HH:mm:ss:fff"));
        }
    }
}
