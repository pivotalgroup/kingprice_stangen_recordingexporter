﻿using PivotalData_RecordingExporter.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PivotalData_RecordingExporter
{
    public class ProcessRecordings : IDisposable
    {
        #region ClassPrep
        public enum ProcessState { Processing, NotStarted, Completed, Error }
        public class ProcessStateEventArgs : EventArgs
        {
            public ProcessState CurrentState { get; set; }
            string Message { get; set; }
        }
        public EventHandler ProcessingStateChanged;

        //Variables

        #endregion ClassPrep

        #region Vars
        Helpers.DBFunctions dbFunc1;
        Helpers.EWSHelper mailClient = new Helpers.EWSHelper();
        bool isConnected = false;
        public List<DBFunctions.DBRow> pendingList { get; set; }
        public List<DBFunctions.DBRow> retryList { get; set; }
        #endregion Vars

        public ProcessRecordings()
        {
            dbFunc1 = new Helpers.DBFunctions();
            var connected = dbFunc1.Connect();

            if (connected)
            {
                StaticFunctions.WriteEventLog("Processing class successfully connected to database.", System.Diagnostics.EventLogEntryType.Information);
                isConnected = true;
            }
            else
            {
                StaticFunctions.WriteEventLog("Processing class could not connect to database, check credentials and network connectivity. There would have been messages before this indicating why the connection failed. Service will wait for next attempt.", System.Diagnostics.EventLogEntryType.Error);
                string message = "ProcessRecordings class was unable to successfully initiate connection to database and could not be instantiated as a result. This is a critical failure and will prevent new recordings from being added to the audit table";
                mailClient.SendEmail(null, null, "Process Recording process could not connect to database", message);
                throw new Exception(message);
            }
        }

        public bool FetchRowsToProcess()
        {
            try
            {
                StaticFunctions.WriteEventLog("Processing class about to attempt fetching of DB rows to process...", System.Diagnostics.EventLogEntryType.Information);
                var rows = dbFunc1.FetchRecordsToProcess();
                if (rows != null)
                {
                    if (rows.Count > 0)
                    {
                        //Sort pending from retry
                        var tempPendingList = rows.FindAll(x => x.Status == "Pending");
                        var tempRetryList = rows.FindAll(x => x.Status == "Retry");

                        if (tempPendingList.Count > 0)
                        {
                            StaticFunctions.WriteEventLog(tempPendingList.Count.ToString() + " pending rows found for processing...", System.Diagnostics.EventLogEntryType.Information);
                            pendingList = new List<DBFunctions.DBRow>();
                            pendingList = tempPendingList;
                        }

                        if (tempRetryList.Count > 0)
                        {
                            StaticFunctions.WriteEventLog(tempRetryList.Count.ToString() + " retry rows found for processing...", System.Diagnostics.EventLogEntryType.Information);
                            retryList = new List<DBFunctions.DBRow>();
                            retryList = tempRetryList;
                        }
                        return true;
                    }
                    else
                    {
                        StaticFunctions.WriteEventLog("Processing run found no new rows to process.", System.Diagnostics.EventLogEntryType.Information);
                        return false;
                    }
                }
                StaticFunctions.WriteEventLog("Processing run found no new rows to process.", System.Diagnostics.EventLogEntryType.Information);
                return false;

            }
            catch (Exception rfe)
            {
                //Exception fetching rows
                string message = "Error while trying to fetch DB rows for processing. Service reaches this point after inserting the latest rows into the audit table. This failure happened while trying to get these rows from the DB for processing and dividing them up between 'Pending' and 'Retry' lists. Exception that was caught: " + rfe.Message;
                StaticFunctions.WriteEventLog(message, System.Diagnostics.EventLogEntryType.Error);
                mailClient.SendEmail(null, null, "Error while fetching rows to process", message);
                return false;
            }
        }

        public void ProcessRows()
        {
            //Create empty lists to prevent exceptions if list are null.
            if (pendingList == null)
            {
                pendingList = new List<DBFunctions.DBRow>();
            }
            if (retryList == null)
            {
                retryList = new List<DBFunctions.DBRow>();
            }

            //Pending
            if (pendingList.Count > 0)
            {
                StaticFunctions.WriteEventLog("Pending rows about to be processed...", System.Diagnostics.EventLogEntryType.Information);
                Helpers.StaticFunctions.ProcessList(pendingList);
                StaticFunctions.WriteEventLog("Pending row processing complete.", System.Diagnostics.EventLogEntryType.Information);
            }

            //Retry
            if (retryList.Count > 0)
            {
                StaticFunctions.WriteEventLog("Retry rows about to be processed...", System.Diagnostics.EventLogEntryType.Information);
                Helpers.StaticFunctions.ProcessList(retryList);
                StaticFunctions.WriteEventLog("Retry row processing complete.", System.Diagnostics.EventLogEntryType.Information);
            }
        }

        public void Dispose()
        {
            //Close all DB connections
            try
            {
                dbFunc1.Disconnect();
            }
            catch (Exception)
            {

            }
            try
            {
                dbFunc1.Dispose();
            }
            catch (Exception)
            {
                //Already Disposed
            }
            try
            {
                dbFunc1 = null;
            }
            catch (Exception)
            {

            }
            
            try
            {
                //mailClient.Dispose();
            }
            catch (Exception)
            {
                //Already disposed
            }
             //mailClient = null;
            //Discard vars
            try
            {
                pendingList.Clear();
            }
            catch (Exception)
            {
                //Already cleared
            }
            try
            {
                pendingList = null;
            }
            catch (Exception)
            {

            }

            try
            {
                retryList.Clear();
            }
            catch (Exception)
            {
                //Already cleared
            }
            try
            {
                retryList = null;
            }
            catch (Exception)
            {

            }
            
        }
    }
}
