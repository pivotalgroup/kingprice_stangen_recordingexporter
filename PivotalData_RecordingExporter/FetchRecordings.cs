﻿using PivotalData_RecordingExporter.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PivotalData_RecordingExporter
{
    public class FetchRecordings : IDisposable
    {
        #region ClassPrep
        Helpers.DBFunctions dbFunc1 = new Helpers.DBFunctions();
        Helpers.EWSHelper mailClient = new EWSHelper();

        //Variables
        bool isConnected = false;
        #endregion ClassPrep

        public FetchRecordings()
        {
            dbFunc1 = new Helpers.DBFunctions();
            var connected = dbFunc1.Connect();

            if (connected)
            {
                StaticFunctions.WriteEventLog("Fetch process successfully connected to database.", System.Diagnostics.EventLogEntryType.Information);
                isConnected = true;
            }
            else
            {
                StaticFunctions.WriteEventLog("Fetch process could not connect to database, check credentials and network connectivity. There would have been messages before this indicating why the connection failed. Service will wait for next attempt.", System.Diagnostics.EventLogEntryType.Error);
                string message = "FetchRecordings class was unable to successfully initiate connection to database and could not be instantiated as a result. This is a critical failure and will prevent new recordings from being added to the audit table";
                mailClient.SendEmail(null, null, "Recording Fetch Process could not connect to database", message);
                throw new Exception(message);
            }
        }

        public string Fetch()
        {
            if (isConnected)
            {
                StaticFunctions.WriteEventLog("Service about to check whether there are any new rows to process.", System.Diagnostics.EventLogEntryType.Information);
                var dataStatus = dbFunc1.PrepareLatestRecordings();

                if (!string.IsNullOrEmpty(dataStatus))
                {
                    if (dataStatus == "Rows Added")
                    {
                        //We have something new to process
                        StaticFunctions.WriteEventLog("Rows found and added to audit table for processing.", System.Diagnostics.EventLogEntryType.Information);
                        return dataStatus;
                    }
                    else
                    {
                        //No New Rows
                        //Nothing new to process, but there could still be unprocessed rows
                        StaticFunctions.WriteEventLog("No new rows were found, Service will wait for next fetch attempt.", System.Diagnostics.EventLogEntryType.Information);
                        return dataStatus;
                    }
                }
            }
            else
            {
                StaticFunctions.WriteEventLog("Attempt to fetch rows were made without a successful DB connection. This should not happen and should be investigated. Service will wait for next fetch attempt.", System.Diagnostics.EventLogEntryType.Warning);
            }
            return "Not Connected";
        }

        public void Dispose()
        {
            try
            {
                dbFunc1.Disconnect();
            }
            catch (Exception)
            {

            }

            try
            {
                dbFunc1.Dispose();
            }
            catch (Exception)
            {
                //Already Disposed
            }
            try
            {
                dbFunc1 = null;
            }
            catch (Exception)
            {

            }
            
            try
            {
                mailClient.Dispose();
            }
            catch (Exception)
            {
                //Already disposed
            }
            try
            {
                mailClient = null;
            }
            catch (Exception)
            {

            }
            
        }
    }
}
