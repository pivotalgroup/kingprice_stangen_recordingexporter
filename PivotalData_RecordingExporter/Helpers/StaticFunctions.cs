﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PivotalData_RecordingExporter.Helpers
{
    public static class StaticFunctions
    {
        public static EventLogEntryType MessageType = new EventLogEntryType();
        public static void WriteEventLog(string Message, EventLogEntryType type)
        {
            try
            {
                int infoCode = 0;
                switch (type)
                {
                    case EventLogEntryType.Error:
                        infoCode = 37767;
                        break;
                    case EventLogEntryType.Warning:
                        infoCode = 9276;
                        break;
                    case EventLogEntryType.Information:
                        infoCode = 4636;
                        break;
                    case EventLogEntryType.SuccessAudit:
                        infoCode = 4636;
                        break;
                    case EventLogEntryType.FailureAudit:
                        infoCode = 4636;
                        break;
                    default:
                        infoCode = 4636;
                        break;
                }


                EventLog.WriteEntry("Pivotal Data Recording Exporter", Message, type, infoCode);
            }
            catch (Exception err)
            {
                //Did not write
            }
        }

        public static void SendEmail(string To, string CC, string Subject, string Message)
        {
            //Generic email feature for notifying parties of errors etc.
            WriteEventLog("Email has been sent out for Subject: " + Subject + ". To participants: " + To, EventLogEntryType.Information);
        }

        public static bool CheckFolderReadWrite(string path)
        {
            //This will only be needed if we can query the S3 in this way, otherwise it will exclusively be for when we move files around after failures
            bool writable = false;
            if (Directory.Exists(path))
            {
                //Path exists
                try
                {
                    System.Security.AccessControl.DirectorySecurity ds = Directory.GetAccessControl(path);
                    writable = true;
                }
                catch (Exception dirae)
                {
                    WriteEventLog("Error while checking write access to folder: " + path + ". Exception caught was: " + dirae.Message + ". This row will be skipped in the service. Please rectify before it retries.", EventLogEntryType.Error);
                    writable = false;
                }
            }
            else
            {
                writable = false;
            }

            return writable;
        }

        public static string FetchWorkingFilePath()
        {
            System.Configuration.ConfigurationManager.RefreshSection("appSettings");
            return System.Configuration.ConfigurationManager.AppSettings["S3WorkingDirectory"];
        }

        public static string FetchRetryFilePath()
        {
            System.Configuration.ConfigurationManager.RefreshSection("appSettings");
            return System.Configuration.ConfigurationManager.AppSettings["S3FailureRetryUNC"];
        }

        public static string FetchFailedFilePath()
        {
            System.Configuration.ConfigurationManager.RefreshSection("appSettings");
            return System.Configuration.ConfigurationManager.AppSettings["S3FailureFinalUNC"];
        }

        public static string FetchIntervalFetch()
        {
            System.Configuration.ConfigurationManager.RefreshSection("appSettings");
            return System.Configuration.ConfigurationManager.AppSettings["LoopFetchInterval"];
        }
        public static string FetchIntervalProcess()
        {
            System.Configuration.ConfigurationManager.RefreshSection("appSettings");
            return System.Configuration.ConfigurationManager.AppSettings["LoopProcessInterval"];
        }

        public static string FetchDefaultUUID()
        {
            System.Configuration.ConfigurationManager.RefreshSection("appSettings");
            return System.Configuration.ConfigurationManager.AppSettings["DefaultUUID"];
        }
        public static string FetchDefaultCaseNo()
        {
            System.Configuration.ConfigurationManager.RefreshSection("appSettings");
            return System.Configuration.ConfigurationManager.AppSettings["DefaultCaseNo"];
        }

        public static int GetDefaultBitRate()
        {
            System.Configuration.ConfigurationManager.RefreshSection("appSettings");
            var strBit = System.Configuration.ConfigurationManager.AppSettings["BitRate"];
            int bitRate = 64;
            try
            {
                bitRate = Convert.ToInt32(strBit);
            }
            catch (Exception)
            {

                bitRate = 64;
            }

            return bitRate;
        }

        public static string GetNextStatus(string status)
        {
            if (status == "Retry")
            {
                return "Failed";
            }
            else
            {
                return "Retry";
            }
        }
        public static string GenFileName(string UUID, string CaseNo, string InteractionId, string Extension)
        {
            if (string.IsNullOrEmpty(UUID))
            {
                try
                {
                    UUID = FetchDefaultUUID();
                }
                catch (Exception fe)
                {
                    UUID = "b43a606e-6f5e-11eb-9439-0242ac130002";
                }
            }
            if (string.IsNullOrEmpty(CaseNo))
            {
                try
                {
                    CaseNo = FetchDefaultCaseNo();
                }
                catch (Exception fe)
                {
                    CaseNo = "0000000";
                }
            }
            string fileName = UUID + "_" + CaseNo + "_" + InteractionId + "." + Extension;
            return fileName;
        }

        public static void ProcessList(List<DBFunctions.DBRow> dbRows)
        {
            var mailClient = new Helpers.EWSHelper();
            try
            {
                using (Helpers.IceLibFunctions libFunc = new Helpers.IceLibFunctions())
                using (Helpers.AmazonS3Functions s3Func = new Helpers.AmazonS3Functions())
                using (Helpers.DBFunctions dbFunc1 = new DBFunctions())
                {
                    string currentState = libFunc.GetCurrentConnectionState();
                    string failureReason = "";
                    string dbUpdateUNC = "";
                    DateTime currentFileDT = DateTime.Now;
                    //Set today as the default folder. Unless there was a failure spanning days, this will always be the correct location
                    string folderName = DateTime.Now.ToString("yyyy") + @"/" + DateTime.Now.ToString("MM") + @"/" + DateTime.Now.ToString("dd");
                    string tempFolderName = "";
                    dbFunc1.Connect();
                    if (currentState == "Up")
                    {
                        libFunc.connectRecorder();
                        foreach (var row in dbRows)
                        {
                            if (row.RecordingDate.HasValue)
                            {
                                currentFileDT = row.RecordingDate.Value;
                                tempFolderName = currentFileDT.ToString("yyyy") + @"/" + currentFileDT.ToString("MM") + @"/" + currentFileDT.ToString("dd");
                                if (folderName == tempFolderName)
                                {
                                    tempFolderName = "";
                                }
                            }
                            else
                            {
                                currentFileDT = DateTime.Now;
                                tempFolderName = "";
                            }
                            dbUpdateUNC = "Could not save media";
                            string tempStatus = "";
                            try
                            {
                                string fileName = StaticFunctions.GenFileName(row.UUID, row.CaseId, row.InteractionId, "wav");
                                string filePath = Helpers.StaticFunctions.FetchWorkingFilePath();
                                if (StaticFunctions.CheckFolderReadWrite(filePath))
                                {
                                    var recDetails = libFunc.StoreFileFromRecId(row.RecordingId, filePath + fileName);
                                    if (!recDetails.StartsWith("Failure,"))
                                    {
                                        //We have our rec in working folder, now we need to convert to MP3
                                        StaticFunctions.WriteEventLog("RecId: " + row.RecordingId + " fetched, about to convert to MP3...", System.Diagnostics.EventLogEntryType.Information);
                                        dbUpdateUNC = filePath + fileName;

                                        try
                                        {
                                            fileName = StaticFunctions.GenFileName(row.UUID, row.CaseId, row.InteractionId, "mp3");
                                            var outcome = MP3Functions.WaveToMP3_FileToFile(recDetails, fileName, StaticFunctions.GetDefaultBitRate());
                                            if (File.Exists(outcome))
                                            {
                                                //Nuke the wave
                                                try
                                                {
                                                    File.Delete(dbUpdateUNC);
                                                }
                                                catch (Exception de)
                                                {
                                                    StaticFunctions.WriteEventLog("Failure while trying to delete wave file. MP3 was successfully created at: " + outcome + ". Service might not have sufficient privelages to delete the file and manual intervention is required. The S3 uploaded about to be attempted. Exception caught was: " + de.Message, System.Diagnostics.EventLogEntryType.Warning);
                                                }

                                                dbUpdateUNC = filePath + fileName;
                                                //We have an MP3, now attempt the upload.
                                                if (string.IsNullOrEmpty(tempFolderName))
                                                {
                                                    tempFolderName = folderName;
                                                }
                                                if (s3Func.CheckBucketAccess(tempFolderName))
                                                {
                                                    //s3Functions.GetBucketList();
                                                    try
                                                    {
                                                        var uploadOutcome = s3Func.UploadFileBytes(File.ReadAllBytes(outcome), fileName, tempFolderName);
                                                        if (uploadOutcome.Contains("Success"))
                                                        {
                                                            try
                                                            {
                                                                File.Delete(outcome);
                                                            }
                                                            catch (Exception de)
                                                            {
                                                                StaticFunctions.WriteEventLog("Failure while trying to delete successfully uploaded file: " + outcome + ". Service might not have sufficient privelages to delete the file and manual intervention is required. The S3 uploaded succeeded" + ". Exception caught was: " + de.Message, System.Diagnostics.EventLogEntryType.Warning);
                                                            }
                                                            dbUpdateUNC = "File Uploaded";
                                                            tempStatus = "Completed";
                                                        }
                                                        else
                                                        {
                                                            tempStatus = StaticFunctions.GetNextStatus(row.Status);
                                                            failureReason = "S3: " + uploadOutcome;
                                                        }
                                                    }
                                                    catch (Exception s3err)
                                                    {
                                                        StaticFunctions.WriteEventLog("Failure while trying to upload recording to S3 bucket. This will count as a failure on the audit log. Recording Id: " + row.RecordingId + ", InteractionId: " + row.InteractionId + ". Exception caught was: " + s3err.Message, System.Diagnostics.EventLogEntryType.Error);
                                                        tempStatus = StaticFunctions.GetNextStatus(row.Status);
                                                        failureReason = s3err.Message;
                                                    }

                                                }
                                                else
                                                {
                                                    tempStatus = StaticFunctions.GetNextStatus(row.Status);
                                                    failureReason = "S3: Bucket Access check came back false. We do not have access to the bucket";
                                                }
                                            }
                                            else
                                            {
                                                StaticFunctions.WriteEventLog("Failure while converting MP3, process completed but file was not found in directory. This will count as a failure on the audit log. Recording Id: " + row.RecordingId + ", InteractionId: " + row.InteractionId + ". Wave file will be used retry if applicable.", System.Diagnostics.EventLogEntryType.Error);
                                                tempStatus = StaticFunctions.GetNextStatus(row.Status);
                                                failureReason = "System IO: Conversion to MP3 failed however no error was observed. Process completed but file was not found. Please investigate";
                                            }
                                        }
                                        catch (Exception mp3e)
                                        {
                                            StaticFunctions.WriteEventLog("Failure while trying to convert wave file to mp3. This will count as a failure on the audit log. Recording Id: " + row.RecordingId + ", InteractionId: " + row.InteractionId + ". Exception caught was: " + mp3e.Message, System.Diagnostics.EventLogEntryType.Error);
                                            tempStatus = StaticFunctions.GetNextStatus(row.Status);
                                            failureReason = mp3e.Message;
                                        }

                                    }
                                    else
                                    {
                                        tempStatus = StaticFunctions.GetNextStatus(row.Status);
                                        failureReason = "PureConnect: " + recDetails;
                                    }
                                }
                                else
                                {
                                    //Cannot access path
                                    StaticFunctions.WriteEventLog("Failure while testing whether filepath is accessible. Please ensure the path of: " + filePath + " is accessible and user the service runs as can write to this location. RecordingId: " + row.RecordingId + ". This will count as a failure on the audit", System.Diagnostics.EventLogEntryType.Error);
                                    tempStatus = StaticFunctions.GetNextStatus(row.Status);
                                    failureReason = "System IO: Unable to access folder " + filePath + " to perform functions. Cannot process recording";
                                }
                            }
                            catch (Exception rece)
                            {
                                //Could not fetch recording
                                StaticFunctions.WriteEventLog("Failure while trying to obtain recording via IceLib and will count as a failure in audit trail. If this is the first failure it will still be retried. Recording Id: " + row.RecordingId + ", InteractionId: " + row.InteractionId + ". Exception caught was: " + rece.Message, System.Diagnostics.EventLogEntryType.Error);
                                tempStatus = StaticFunctions.GetNextStatus(row.Status);
                                failureReason = "General: Failure while trying to process recording, error was: " + rece.Message;
                            }

                            //Update with new status and move file if needed
                            string failurePath = "";
                            string exception = "";
                            string subject = "";
                            string message = "";
                            switch (tempStatus)
                            {
                                case "Retry":
                                    if (Directory.Exists(dbUpdateUNC))
                                    {
                                        //There is a file somewhere, move to retry
                                        failurePath = StaticFunctions.FetchRetryFilePath();
                                        try
                                        {
                                            File.Move(dbUpdateUNC, failurePath + Path.GetFileName(dbUpdateUNC));
                                            dbUpdateUNC = failurePath + Path.GetFileName(dbUpdateUNC);
                                        }
                                        catch (Exception pe)
                                        {
                                            StaticFunctions.WriteEventLog("Failure while trying to move file to retry folder. Database will be stamped with the current path however manual intervention is required to ensure good file management. From path: " + dbUpdateUNC + ", To path: " + failurePath + Path.GetFileName(dbUpdateUNC) + ". Exception caught was: " + pe.Message, System.Diagnostics.EventLogEntryType.Warning);
                                            exception = pe.Message;
                                        }
                                    }
                                    else
                                    {
                                        //No file to move
                                    }

                                    //Send email
                                    subject = "Failed to upload recording, recording moved to status " + tempStatus + ". Recording: " + Path.GetFileName(dbUpdateUNC);
                                    message = Path.GetFileName(dbUpdateUNC) +
                                                    " failed to upload, this could be due to a local storage issue, connectivity to PureConnect or S3 bucket. Details are:" + Environment.NewLine +
                                                    "Retry From and To path: " + dbUpdateUNC + " to " + failurePath + Path.GetFileName(dbUpdateUNC) + Environment.NewLine +
                                                    "Failure reason captured: " + failureReason + Environment.NewLine +
                                                    "Exception while moving/processing file for next retry attempt if any: " + exception;
                                    if (tempStatus == "Failed")
                                    {
                                        message += Environment.NewLine + "Recording was moved to a Failed state and will not be retried. Manual intervention required.";
                                    }
                                    mailClient.SendEmail(null, null, subject, message);

                                    break;
                                case "Failed":
                                    if (Directory.Exists(dbUpdateUNC))
                                    {
                                        //There is a file somewhere, move to retry
                                        failurePath = StaticFunctions.FetchFailedFilePath();
                                        try
                                        {
                                            File.Move(dbUpdateUNC, failurePath + Path.GetFileName(dbUpdateUNC));
                                            dbUpdateUNC = failurePath + Path.GetFileName(dbUpdateUNC);
                                        }
                                        catch (Exception pe)
                                        {
                                            StaticFunctions.WriteEventLog("Failure while trying to move file to failure folder. Database will be stamped with the current path however manual intervention is required to ensure good file management. From path: " + dbUpdateUNC + ", To path: " + failurePath + Path.GetFileName(dbUpdateUNC) + ". Exception caught was: " + pe.Message, System.Diagnostics.EventLogEntryType.Warning);
                                            exception = pe.Message;
                                        }
                                    }
                                    else
                                    {
                                        //No file to move (Could be 2nd failed attempt at downloading media so no file ever existed)
                                    }
                                    //Send email
                                    subject = "Failed to upload recording, recording moved to status " + tempStatus + ". Recording: " + Path.GetFileName(dbUpdateUNC);
                                    message = Path.GetFileName(dbUpdateUNC) +
                                                    " failed to upload, this could be due to a local storage issue, connectivity to PureConnect or S3 bucket. Details are:" + Environment.NewLine +
                                                    "Failed From and To path: " + dbUpdateUNC + " to " + failurePath + Path.GetFileName(dbUpdateUNC) + Environment.NewLine +
                                                    "Failure reason captured: " + failureReason + Environment.NewLine +
                                                    "Exception while moving/processing file for next retry attempt if any: " + exception;
                                    if (tempStatus == "Failed")
                                    {
                                        message += Environment.NewLine + "Recording was moved to a Failed state and will not be retried. Manual intervention required.";
                                    }
                                    mailClient.SendEmail(null, null, subject, message);
                                    break;
                                default:
                                    //Else do nothing
                                    break;
                            }

                            //File management done, update DB
                            try
                            {
                                dbFunc1.UpdateStatus(row.Index.Value, tempStatus);
                                dbFunc1.UpdateUNC(row.Index.Value, dbUpdateUNC);
                            }
                            catch (Exception dbe)
                            {
                                StaticFunctions.WriteEventLog("Failure while trying to update database with latest status of: " + tempStatus + " for RecId: " + row.RecordingId + " with UNC path value of: " + dbUpdateUNC + ". Statements could have partially succeeded, please investigate and manually update database to recover. Exception caught was: " + dbe.Message, System.Diagnostics.EventLogEntryType.Error);
                            }
                        }
                    }
                    else
                    {
                        //Something is wrong, we did not connect to PureConnect correctly, abandon
                        StaticFunctions.WriteEventLog("Processing run detected that connection attempt to PureConnect was not successful, there should be messages before this one indicating why it failed. This message signifies that the current run will be skipped as no recordings can be fetched. Will retry on next run.", System.Diagnostics.EventLogEntryType.Error);
                    }
                }
            }
            catch (Exception pre)
            {
                //Failed to process

                string subject = "General failure while trying to process recordings";
                string message = "General failure while trying to process recordings. A general unhandled exception was caught during the process run. This is not a per recording error, but indicative of some time of failure not explicitly catered for. Investigation required. Exception was: " + pre.Message;
                mailClient.SendEmail(null, null, subject, message);

                StaticFunctions.WriteEventLog(message, System.Diagnostics.EventLogEntryType.Error);
            }
        }

    }
}
