﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//Added namespaces
using System.IO;
using NAudio.Wave;
using NAudio.Lame;

namespace PivotalData_RecordingExporter.Helpers
{
    /// <summary>
    /// Due to the type of wave file, NAudio cannot process the wave file directly 
    /// (As you have to specify the exact attributes of the wave file and the wave from PureConnect is technically not supported)
    /// So we save the wave file to disk and then ask NAudio to process from there which allows it to take a crack at converting without interpreting the wave file which works
    /// ** TO DO - Client wants OPUS conversion. Investigation needed.
    /// </summary>
    public static class MP3Functions
    {
        // Convert WAV to MP3 using libmp3lame library
        public static void WaveToMP3_WriteFile(string waveFileName, string mp3FileName, int bitRate = 128)
        {
            using (var reader = new AudioFileReader(waveFileName))
            using (var writer = new LameMP3FileWriter(mp3FileName, reader.WaveFormat, bitRate))
                reader.CopyTo(writer);


        }

        // Convert MP3 file to WAV using NAudio classes only
        public static void MP3ToWave_WriteFile(string mp3FileName, string waveFileName)
        {
            using (var reader = new Mp3FileReader(mp3FileName))
            using (var writer = new WaveFileWriter(waveFileName, reader.WaveFormat))
                reader.CopyTo(writer);
        }
        public static byte[] WaveToMP3_BytesToBytes(byte[] waveFile, string mp3FileName, int bitRate = 128)
        {
            byte[] byteArr;
            System.IO.Stream stream = new System.IO.MemoryStream();
            System.IO.Stream waveStream = new System.IO.MemoryStream(waveFile);
            using (var reader = new WaveFileReader(waveStream))
            using (var writer = new LameMP3FileWriter(stream, reader.WaveFormat, bitRate))
                reader.CopyTo(writer);

            byteArr = ReadFully(stream);

            return byteArr;
        }

        public static string WaveToMP3_FileToFile(string waveFile, string mp3FileName, int bitRate = 128)
        {
            try
            {
                string newPath = Path.GetDirectoryName(Path.GetFullPath(waveFile));
                newPath += @"\";
                using (var reader = new AudioFileReader(waveFile))
                using (var writer = new LameMP3FileWriter(newPath + mp3FileName, reader.WaveFormat, bitRate))
                {
                    reader.CopyTo(writer);
                    return newPath + mp3FileName;
                }
            }
            catch (Exception encerr)
            {
                throw;
            }
        }

        private static byte[] ReadFully(Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }


    }
}
