﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PivotalData_RecordingExporter.Helpers
{
    public class DBFunctions : IDisposable
    {
        string _dbServer, _dbName, _dbUser, _dbPass, _dbTimeout;
        int HoursPeriod = 0;
        int UUIDAtt = 0;
        int CaseNoAtt = 0;
        string QualifierAtt = "";
        private SqlConnection _con = new SqlConnection();

        public class DBRow
        {
            public int? Index { get; set; }
            public string RecordingId { get; set; }
            public string InteractionId { get; set; }
            public string Status { get; set; }
            public int? Attempts { get; set; }
            public string CurrentUNC { get; set; }
            public string UUID { get; set; }
            public string CaseId { get; set; }
            public DateTime? InsertedDateTime { get; set; }
            public DateTime? ModifiedDateTime { get; set; }
            public DateTime? CompletedDateTime { get; set; }
            public string MediaURI { get; set; }
            public DateTime? RecordingDate { get; set; }
            public string InitiationPolicy { get; set; }

        }
        public DBFunctions()
        {
            ConfigurationManager.RefreshSection("appSettings");
            _dbServer = ConfigurationManager.AppSettings["DBServer"];
            _dbName = ConfigurationManager.AppSettings["DBName"];
            _dbUser = ConfigurationManager.AppSettings["DBUser"];
            _dbPass = ConfigurationManager.AppSettings["DBPass"];
            _dbTimeout = ConfigurationManager.AppSettings["DBTimeout"];

            string qualAtt = ConfigurationManager.AppSettings["QualifierAtt"];
            string uuidAtt = ConfigurationManager.AppSettings["UUIDAtt"];
            string casenoAtt = ConfigurationManager.AppSettings["CaseNoAtt"];
            string hoursSearch = ConfigurationManager.AppSettings["HoursToSearch"];

            try
            {
                HoursPeriod = Convert.ToInt32(hoursSearch);
                UUIDAtt = Convert.ToInt32(uuidAtt);
                CaseNoAtt = Convert.ToInt32(casenoAtt);
                QualifierAtt = qualAtt;
            }
            catch (Exception e)
            {
                //Use defaults
                HoursPeriod = 48;
                UUIDAtt = 20;
                CaseNoAtt = 19;
                QualifierAtt = "18";
            }
        }

        public bool Connect()
        {
            _con = new SqlConnection();
            if (_dbUser.Contains(@"\"))
            {
                _con = new SqlConnection("user id=" + _dbUser + "; " +
                                                    "password=" + _dbPass + "; " +
                                                    "server=" + _dbServer + "; " +
                                                    //"Trusted_Connection=yes;" +
                                                    "Integrated Security=SSPI;" +
                                                    "database=" + _dbName);
            }
            else
            {
                _con = new SqlConnection("user id=" + _dbUser + "; " +
                                                    "password=" + _dbPass + "; " +
                                                    "server=" + _dbServer + "; " +
                                                    //"Trusted_Connection=yes;" +
                                                    //"Integrated Security=SSPI;" +
                                                    "database=" + _dbName);
            }

            try
            {
                _con.Open();
                return true;
            }
            catch (Exception e)
            {

                //Could not open connection
                return false;
            }
        }

        public bool Disconnect()
        {
            try
            {
                _con.Close();
                _con.Dispose();
                return true;
            }
            catch (Exception ce)
            {
                return false;
            }

        }

        public string PrepareLatestRecordings()
        {
            if (_con.State == System.Data.ConnectionState.Closed)
            {
                Connect();
            }

            SqlCommand cmd = new SqlCommand("Pivotal_SP_SL_PrepareUpload", _con);

            try
            {
                cmd.CommandTimeout = Convert.ToInt32(_dbTimeout);
            }
            catch (Exception)
            {
                cmd.CommandTimeout = 320;
            }

            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SearchPeriod", HoursPeriod);
            cmd.Parameters.AddWithValue("@QualifierAttribute", QualifierAtt);
            cmd.Parameters.AddWithValue("@UUIDAttribute", UUIDAtt);
            cmd.Parameters.AddWithValue("@CaseNoAttribute", CaseNoAtt);


            try
            {
                var responseMessage = cmd.ExecuteScalar();

                if (responseMessage != null)
                {
                    return responseMessage.ToString();
                }
                else
                {
                    return "Failure|Empty response";
                }
            }
            catch (Exception ee)
            {

                //Failed, nothing we can do
                return "Failure|" + ee.Message;
            }
        }


        public List<DBRow> FetchRecordsToProcess()
        {
            if (_con.State == System.Data.ConnectionState.Closed)
            {
                Connect();
            }
            List<DBRow> recs = new List<DBRow>();

            SqlCommand cmd = new SqlCommand("Pivotal_SP_SL_FetchRecsToPush", _con);

            try
            {
                cmd.CommandTimeout = Convert.ToInt32(_dbTimeout);
            }
            catch (Exception)
            {
                cmd.CommandTimeout = 320;
            }

            cmd.CommandType = System.Data.CommandType.StoredProcedure;

            try
            {
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    try
                    {
                        DateTime? tempMod = DateTime.Now;
                        DateTime? tempComp = DateTime.Now;
                        if (reader[9] == DBNull.Value)
                        {
                            tempMod = null;
                        }
                        else
                        {
                            tempMod = reader.GetFieldValue<DateTime>(9);
                        }

                        if (reader[10] == DBNull.Value)
                        {
                            tempComp = null;
                        }
                        else
                        {
                            tempComp = reader.GetFieldValue<DateTime>(10);
                        }
                        var tempRow = new DBRow()
                        {
                            Index = Convert.ToInt32(reader[0]),
                            RecordingId = reader[1].ToString(),
                            InteractionId = reader[2].ToString(),
                            Status = reader[3].ToString(),
                            Attempts = Convert.ToInt32(reader[4]),
                            CurrentUNC = reader[5].ToString(),
                            UUID = reader[6].ToString(),
                            CaseId = reader[7].ToString(),
                            InsertedDateTime = reader.GetFieldValue<DateTime>(8),
                            ModifiedDateTime = tempMod,
                            CompletedDateTime = tempComp,
                            MediaURI = reader[11].ToString(),
                            RecordingDate = reader.GetFieldValue<DateTime>(12),
                            InitiationPolicy = reader[13].ToString()
                        };

                        recs.Add(tempRow);
                    }
                    catch (Exception err)
                    {
                        //Failed to fetch rown, possibly bad data or format
                    }
                }
            }
            catch (Exception ee)
            {

                //Failed, nothing we can do
                return null;
            }

            return recs;
        }

        public void UpdateStatus(int IndexVal, string StatusVal)
        {
            if (_con.State == System.Data.ConnectionState.Closed)
            {
                Connect();
            }

            SqlCommand cmd = new SqlCommand("Pivotal_SP_SL_UpdateStatus", _con);

            try
            {
                cmd.CommandTimeout = Convert.ToInt32(_dbTimeout);
            }
            catch (Exception)
            {
                cmd.CommandTimeout = 320;
            }

            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Index", IndexVal);
            cmd.Parameters.AddWithValue("@Status", StatusVal);

            try
            {
                try
                {
                    var rowsUpdated = cmd.ExecuteNonQuery();

                    if (rowsUpdated > 0)
                    {
                        //Something was updated!
                    }
                }
                catch (Exception ee)
                {

                    //Failed, nothing we can do
                }
            }
            catch (Exception ee)
            {
                //Failed, nothing we can do
            }

        }

        public void UpdateUNC(int IndexVal, string UNCVal)
        {
            if (_con.State == System.Data.ConnectionState.Closed)
            {
                Connect();
            }

            if (_con.State == System.Data.ConnectionState.Closed)
            {
                Connect();
            }

            SqlCommand cmd = new SqlCommand("Pivotal_SP_SL_UpdateUNC", _con);

            try
            {
                cmd.CommandTimeout = Convert.ToInt32(_dbTimeout);
            }
            catch (Exception)
            {
                cmd.CommandTimeout = 320;
            }

            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Index", IndexVal);
            cmd.Parameters.AddWithValue("@UNCPath", UNCVal);

            try
            {
                try
                {
                    var rowsUpdated = cmd.ExecuteNonQuery();

                    if (rowsUpdated > 0)
                    {
                        //Something was updated!
                    }
                }
                catch (Exception ee)
                {

                    //Failed, nothing we can do
                }
            }
            catch (Exception ee)
            {
                //Failed, nothing we can do
            }

        }

        public void Dispose()
        {
            try
            {
                _con.Close();

            }
            catch (Exception)
            {
                //
            }

            try
            {
                _con.Dispose();
            }
            catch (Exception)
            {
                //Already disposed
            }
            try
            {
                _con = null;
            }
            catch (Exception)
            {

            }
            
        }
    }
}
