﻿using ININ.IceLib;
using ININ.IceLib.Connection;
using ININ.IceLib.Data.TransactionBuilder;
using ININ.IceLib.QualityManagement;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;

namespace PivotalData_RecordingExporter.Helpers
{
    public class IceLibFunctions : IDisposable
    {
        private Session _session;
        private QualityManagementManager _QualityManagementManager;
        private RecordingsManager _RecordingsManager;

        SqlConnection _con = new SqlConnection();

        string _serv1, _serv2, _user, _pass, _dbServer, _dbName, _dbUser, _dbPass, _dbTimeout, _workDir;

        public EventHandler<ConnectionStateChangedEventArgs> ConnectionStateChanged;

        public class RecordingData
        {
            int? DBIndex { get; set; }
            string RecordingId { get; set; }
            string InteractionId { get; set; }
            string CaseNumber { get; set; }
            Guid StangenUUID { get; set; }
            string UserExt { get; set; }
        }

        public IceLibFunctions(string Server1, string Server2, string User, string Pass)
        {
            _serv1 = Server1;
            _serv2 = Server2;
            _user = User;
            _pass = Pass;

            //Add the refresh config section command here
            System.Configuration.ConfigurationManager.RefreshSection("appSettings");
            _dbServer = System.Configuration.ConfigurationManager.AppSettings["DBServer"];
            _dbName = System.Configuration.ConfigurationManager.AppSettings["DBName"];
            _dbUser = System.Configuration.ConfigurationManager.AppSettings["DBUser"];
            _dbPass = System.Configuration.ConfigurationManager.AppSettings["DBPass"];
            _dbTimeout = System.Configuration.ConfigurationManager.AppSettings["DBTimeout"];
            _workDir = System.Configuration.ConfigurationManager.AppSettings["S3WorkingDirectory"];

            Login();
        }

        public IceLibFunctions()
        {

            System.Configuration.ConfigurationManager.RefreshSection("appSettings");
            _dbServer = System.Configuration.ConfigurationManager.AppSettings["DBServer"];
            _dbName = System.Configuration.ConfigurationManager.AppSettings["DBName"];
            _dbUser = System.Configuration.ConfigurationManager.AppSettings["DBUser"];
            _dbPass = System.Configuration.ConfigurationManager.AppSettings["DBPass"];
            _dbTimeout = System.Configuration.ConfigurationManager.AppSettings["DBTimeout"];
            _workDir = System.Configuration.ConfigurationManager.AppSettings["S3WorkingDirectory"];

            _serv1 = System.Configuration.ConfigurationManager.AppSettings["PCServer1"];
            _serv2 = System.Configuration.ConfigurationManager.AppSettings["PCServer2"];
            _user = System.Configuration.ConfigurationManager.AppSettings["PCUser"];
            _pass = System.Configuration.ConfigurationManager.AppSettings["PCPass"];

            Login();
        }

        public string GetCurrentConnectionState()
        {
            switch (_session.ConnectionState)
            {
                case ININ.IceLib.Connection.ConnectionState.None:
                    return "None";
                    break;
                case ININ.IceLib.Connection.ConnectionState.Up:
                    return "Up";
                    break;
                case ININ.IceLib.Connection.ConnectionState.Down:
                    return "Down";
                    break;
                case ININ.IceLib.Connection.ConnectionState.Attempting:
                    return "Attempting";
                    break;
                default:
                    return "Unknown";
                    break;
            }
        }

        public void Login()
        {
            // Introducing adding your own tracing to the log
            Tracing.TraceAlways("Client logging in");

            // The normal login routine
            _session = new Session();

            _session.ConnectionStateChanged += _session_ConnectionStateChanged;

            try
            {
                //Server1
                AuthSettings auth = new ICAuthSettings(_user, _pass);
                _session.Connect(new SessionSettings(), new HostSettings(new HostEndpoint(_serv1)),
                   auth, new StationlessSettings());

                //_session.Connect(new SessionSettings(), new HostSettings(new HostEndpoint(System.Web.Configuration.WebConfigurationManager.AppSettings["cicserver"])),
                //   auth, new WorkstationSettings(System.Web.Configuration.WebConfigurationManager.AppSettings["cicstation"], SupportedMedia.Call));
                string sessionMessage = _session.ConnectionStateMessage;
                Tracing.TraceAlways("Pivotal Data Recording Exporter, session successfully established. State: " + sessionMessage);
            }
            catch (Exception e)
            {

                try
                {
                    AuthSettings auth = new ICAuthSettings(_user, _pass);
                    _session.Connect(new SessionSettings(), new HostSettings(new HostEndpoint(_serv2)),
                       auth, new StationlessSettings());

                    //_session.Connect(new SessionSettings(), new HostSettings(new HostEndpoint(System.Web.Configuration.WebConfigurationManager.AppSettings["cicserver"])),
                    //   auth, new WorkstationSettings(System.Web.Configuration.WebConfigurationManager.AppSettings["cicstation"], SupportedMedia.Call));
                    string sessionMessage = _session.ConnectionStateMessage;
                    Tracing.TraceAlways("Pivotal Data Recording Exporter, session successfully established. State: " + sessionMessage);
                }
                catch (Exception a3)
                {
                    Tracing.TraceException(a3, "Pivotal Data Recording Exporter, failure while trying to open session on IceLib to PureConnect. Both servers " + _serv1 + " and " + _serv2 + " were attempted without success.");

                    throw;
                }
            }
        }

        private void _session_ConnectionStateChanged(object sender, ConnectionStateChangedEventArgs e)
        {
            try
            {
                ConnectionStateChanged?.Invoke(this, e);
            }
            catch (Exception ece)
            {

                //Do nothing, if this fails it means nothing is subscribed to the events
            }
        }

        public void disconnect()
        {
            try
            {
                _session.Disconnect();
            }
            catch (Exception s)
            {
                //Crap

            }

        }

        public bool connectRecorder()
        {
            // Now that we have everything, let's load up what we need to get recordings downloaded
            try
            {
                //new Task(() => { _QualityManagementManager = QualityManagementManager.GetInstance(_session); });
                //Task getRec = new Task(() => { QualityManagementManager.GetInstance(_session); });

                Task<QualityManagementManager> qmm = Task<QualityManagementManager>.Factory.StartNew(() =>
                {
                    return QualityManagementManager.GetInstance(_session);
                });

                _QualityManagementManager = qmm.Result;

                //getRec.Start();
                //Task.WaitAll(new Task[] { getRec });

                // _QualityManagementManager = QualityManagementManager.GetInstance(_session);
                _RecordingsManager = _QualityManagementManager.RecordingsManager;

                return true;
            }
            catch (Exception e)
            {

                return false;
            }

        }

        public List<RecordingData> GetRecordingList()
        {
            //This method triggers stored proc that fetches custom and standard recording data into a single row for processing.

            List<RecordingData> latestRecs = new List<RecordingData>();

            return latestRecs;
        }

        public void UploadRecording(string recId)
        {
            /*This should contain: 
             * The URI fetch from PureConnect
             * MP3 conversion
             * Upload to S3 bucket
             * Error handling on all of the above
             * For 1 recording at a time, with a external process covering the loop of files
             */

            //Get the Recording
            Uri tempURI = _RecordingsManager.GetExportUri(recId, RecordingMediaType.PrimaryMedia, "", 0);


            //Get file extension
            string[] segments = tempURI.Segments;
            string filename = segments.Length == 0 ? "" : segments[segments.Length - 1];
            string extension = Path.GetExtension(filename);
            //filename = callIDArray[index] + ".sasf";
            filename = recId + ".wav";
            WebClient web1 = new WebClient();
            // web1.DownloadFile(tempURI, uncPath + filename);
            var mediaBytes = web1.DownloadData(tempURI);

        }

        public byte[] GetMediaBytesFromRecId(string recId)
        {
            //Get the Recording
            Uri tempURI = _RecordingsManager.GetExportUri(recId, RecordingMediaType.PrimaryMedia, "", 0);


            //Get file extension
            string[] segments = tempURI.Segments;
            string filename = segments.Length == 0 ? "" : segments[segments.Length - 1];
            string extension = Path.GetExtension(filename);
            //filename = callIDArray[index] + ".sasf";
            filename = recId + ".wav";
            WebClient web1 = new WebClient();
            // web1.DownloadFile(tempURI, uncPath + filename);
            var mediaBytes = web1.DownloadData(tempURI);

            return mediaBytes;
        }

        public string StoreFileFromRecId(string recId, string fileName)
        {
            try
            {
                //string tempFilePath = _workDir;
                //tempFilePath += fileName;
                string tempFilePath = fileName;
                //Get the Recording
                Uri tempURI = _RecordingsManager.GetExportUri(recId, RecordingMediaType.PrimaryMedia, "", 0);
                //Uri debugURI = new Uri("http://10.241.31.206:8106" + tempURI.AbsolutePath);
                //tempURI = debugURI;

                //Get file extension
                string[] segments = tempURI.Segments;
                string filename = segments.Length == 0 ? "" : segments[segments.Length - 1];
                string extension = Path.GetExtension(filename);
                //filename = callIDArray[index] + ".sasf";
                filename = recId + ".wav";
                WebClient web1 = new WebClient();

                try
                {
                    web1.DownloadFile(tempURI, tempFilePath);
                    return tempFilePath;
                }
                catch (Exception eek)
                {
                    StaticFunctions.WriteEventLog("Failure while trying to store RecId: " + recId + " in path: " + tempFilePath + ". Exception caught was: " + eek.Message, System.Diagnostics.EventLogEntryType.Error);
                    return "Failure, " + eek.Message;
                }
            }
            catch (Exception proce)
            {
                StaticFunctions.WriteEventLog("Failure while trying to store RecId: " + recId + " in path: " + fileName + ". Exception caught was: " + proce.Message, System.Diagnostics.EventLogEntryType.Error);
                return "Failure, " + proce.Message;
            }
        }

        public void Dispose()
        {
            //EventHandler
            try
            {
                ConnectionStateChanged = null;
            }
            catch (Exception)
            {

            }
            //SQL
            try
            {
                if (_con.State != System.Data.ConnectionState.Closed)
                {
                    _con.Close();
                }
                _con.Dispose();
                _con = null;
            }
            catch (Exception)
            {

            }

            //Rec
            try
            {
                if (_QualityManagementManager.Session.ConnectionState != ININ.IceLib.Connection.ConnectionState.Down)
                {
                    _QualityManagementManager.Session.Disconnect();
                    _QualityManagementManager.Session.Dispose();
                }
                _RecordingsManager = null;
                _QualityManagementManager = null;
            }
            catch (Exception)
            {

            }

            //IC
            try
            {
                if (_session.ConnectionState != ININ.IceLib.Connection.ConnectionState.Down)
                {
                    _session.Disconnect();
                    _session.Dispose();
                }
                _session = null;
            }
            catch (Exception)
            {

            }
        }
    }
}
