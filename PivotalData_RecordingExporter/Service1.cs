﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

//Added Namespaces
using PivotalData_RecordingExporter.Helpers;

namespace PivotalData_RecordingExporter
{
    public partial class Service1 : ServiceBase
    {
        Timer timerFetch = new Timer();
        Timer timerProcess = new Timer();
        bool CurrentlyProcessing = false;
        bool CurrentlyFetching = false;
        string LastFetchTimerInterval = "";
        string LastProcessTimerInterval = "";
        ProcessRecordings processRecordings;
        FetchRecordings fetchRecordings;
        public Service1()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Start up only manages initial kick of the timer. The main class is created and destroyed on each run to assist with resource management and closing of possible errant DB connections
        /// </summary>
        /// <param name="args"></param>
        protected override void OnStart(string[] args)
        {
            StaticFunctions.WriteEventLog("Service has started.", EventLogEntryType.Information);
            StaticFunctions.WriteEventLog("Timer that governs fetching of records to process has commenced", EventLogEntryType.Information);
            //Kick off fetch timer
            timerFetch.Elapsed += Timer_Elapsed;
            //timerFetch.Interval = 5000; 
            timerFetch.Interval = 10000;
            LastFetchTimerInterval = "5000";
            timerFetch.Enabled = true;

            StaticFunctions.WriteEventLog("Timer that governs processing of records has commenced", EventLogEntryType.Information);
            //Kick off the processing timer
            timerProcess.Elapsed += TimerProcess_Elapsed;
            //timerProcess.Interval = 15000;
            timerProcess.Interval = 30000;
            LastProcessTimerInterval = "15000";
            timerProcess.Enabled = true;

        }

        /// <summary>
        /// Primary loop of the application for fetching recordings. Each run consists of:
        /// 1. setting the timer interval, 2. Instantiating the main class and subscribing to events for it, 
        /// 3. Processing recordings and finally 4. Cleanup before waiting for next interval
        /// This process will continue to run provided there are records to process. Only when it runs out of records to process will it wait for the next interval
        /// 'Retry' status records will not be eligible to retry unless they are at least 10 minutes old at time of fetching. This ensures an ongoing loop does not retry 
        /// these records immediately during a busy period where the service can continually process.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TimerProcess_Elapsed(object sender, ElapsedEventArgs e)
        {
            #region TimerManagement
            //Get the normal timer
            string TimerInterval = "";
            try
            {
                TimerInterval = StaticFunctions.FetchIntervalProcess();
                if (LastProcessTimerInterval != TimerInterval)
                {
                    timerProcess.Interval = Convert.ToInt32(TimerInterval);
                    StaticFunctions.WriteEventLog("Process timer Interval has been updated to " + TimerInterval + " from " + LastProcessTimerInterval + ".", EventLogEntryType.Information);
                    LastProcessTimerInterval = TimerInterval;
                }
            }
            catch (Exception te)
            {
                timerProcess.Interval = 600000;
                StaticFunctions.WriteEventLog("Exception caught while trying to update process timer interval. Message was: " + te.Message, EventLogEntryType.Error);
            }
            #endregion TimerManagement

            if (!CurrentlyProcessing)
            {
                CurrentlyProcessing = true;
                StaticFunctions.WriteEventLog("Processing run has commenced. If there is enough volume between fetch runs, this run will continue until it runs out of records to process... ", EventLogEntryType.Information);
                bool canProcess = false;

                #region Events

                #endregion Events

                #region Loop
                try
                {
                    processRecordings = new ProcessRecordings();
                    canProcess = processRecordings.FetchRowsToProcess();

                    while (canProcess)
                    {
                        //Perform a run
                        processRecordings.ProcessRows();
                        //Done, dispose variables before processing more
                        try
                        {
                            processRecordings.Dispose();
                        }
                        catch (Exception clee)
                        {

                        }
                        try
                        {
                            processRecordings = null;
                        }
                        catch (Exception clev)
                        {

                        }
                        if (processRecordings == null)
                        {
                            processRecordings = new ProcessRecordings();
                        }

                        //Check if more rows
                        canProcess = processRecordings.FetchRowsToProcess();
                    }
                }
                catch (Exception err)
                {
                    //Failure
                    StaticFunctions.WriteEventLog("Processing run of recordings experienced a failure and will not process new recordings for this run. Exception caught while trying to process: " + err.Message, EventLogEntryType.Error);
                }
                #endregion Loop

                #region Cleanup
                try
                {
                    processRecordings.Dispose();
                    processRecordings = null;
                }
                catch (Exception clee)
                {

                }
                #endregion Cleanup
                StaticFunctions.WriteEventLog("Processing run has completed. This indicates it ran out of records to process and will wait for next run. The current interval is " + TimerInterval + " milliseconds however the timer runs asynchronously so the next trigger could be in less time than this interval.", EventLogEntryType.Information);
                CurrentlyProcessing = false;
            }
            else
            {
                StaticFunctions.WriteEventLog("Processing run interval triggered however the previous run is still processing and will not attempt another for this interval. Process and will wait for " + TimerInterval + " milliseconds before attempting again.", EventLogEntryType.Information);
            }
        }

        /// <summary>
        /// Primary loop of the application for fetching recordings. Each run consists of:
        /// 1. setting the timer interval, 2. Instantiating the main class, 
        /// 3. Fetching recordings to process into audit table and finally 4. Cleanup before waiting for next interval
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            #region TimerManagement
            string TimerInterval = "";
            try
            {
                TimerInterval = StaticFunctions.FetchIntervalFetch();
                if (LastFetchTimerInterval != TimerInterval)
                {
                    timerFetch.Interval = Convert.ToInt32(TimerInterval);
                    StaticFunctions.WriteEventLog("Fetch timer Interval has been updated to " + TimerInterval + " from " + LastFetchTimerInterval + ".", EventLogEntryType.Information);
                    LastFetchTimerInterval = TimerInterval;
                }
            }
            catch (Exception te)
            {
                timerFetch.Interval = 600000;
                StaticFunctions.WriteEventLog("Exception caught while trying to update fetch timer interval. Message was: " + te.Message, EventLogEntryType.Error);
            }
            #endregion TimerManagement

            if (!CurrentlyFetching)
            {
                CurrentlyFetching = true;
                StaticFunctions.WriteEventLog("Fetching run has commenced. This will fetch the latest records for processing and add it to the audit table where the processing process will action... ", EventLogEntryType.Information);

                #region Loop
                try
                {
                    fetchRecordings = new FetchRecordings();
                    var rowsStatus = fetchRecordings.Fetch();
                    StaticFunctions.WriteEventLog("Fetch process returned " + rowsStatus, EventLogEntryType.Information);
                }
                catch (Exception err)
                {
                    //Failure
                    StaticFunctions.WriteEventLog("Fetch process experienced a failure and will not fetch new recordings for this run. Exception caught while trying to fetch new rows: " + err.Message, EventLogEntryType.Error);
                }
                #endregion Loop

                #region Cleanup
                fetchRecordings.Dispose();
                fetchRecordings = null;
                StaticFunctions.WriteEventLog("Fetching run has completed. The current interval is " + TimerInterval + " milliseconds however the timer runs asynchronously so the next trigger to look for new rows could be in less time than this interval.", EventLogEntryType.Information);
                CurrentlyFetching = false;
                #endregion Cleanup
            }
            else
            {
                StaticFunctions.WriteEventLog("Fetching run interval triggered however the previous run is still processing and will not attempt another for this interval. Process and will wait for " + TimerInterval + " milliseconds before attempting again.", EventLogEntryType.Information);
            }
        }

        protected override void OnStop()
        {
            try
            {
                processRecordings.Dispose();
            }
            catch (Exception)
            {
                //Already disposed
            }
            
            processRecordings = null;
            try
            {
                fetchRecordings.Dispose();
            }
            catch (Exception)
            {
                //Already disposed
            }
            fetchRecordings = null;
            StaticFunctions.WriteEventLog("Service has stopped", EventLogEntryType.Information);
        }
    }
}
